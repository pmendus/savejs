# README #

## SaveJS ###

SaveJS is a JavaScript library that allows users to create save states of particular JSON data to allow the inclusion of light persistence without the need for sessions in a static website.

Version: 0.0.1

### Created by Patrick Mendus ###

#### 12/2/2021 ####
#### patmendus@patmendus.com ####